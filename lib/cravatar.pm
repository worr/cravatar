package cravatar;

use warnings;
use strict;
use v5.18;

use Dancer ':syntax';
use Dancer::Plugin::LDAP;
use Dancer::Plugin::Cache::CHI;
use GD::Thumbnail;
use MIME::Type;
use Net::LDAP::Util qw/escape_filter_value/;
use Try::Tiny;

our $VERSION = '0.5';

get '/' => sub {
    redirect '/upload';
};

sub get_photo {
    my $uuid = shift;
    my $ret = cache_get $uuid;
    if (defined $ret) {
        return $ret;
    }

    # we get an array of jpegPhotos, so let's grab the first one
    try {
        $ret = ldap->search(
            base => "ou=Users,dc=csh,dc=rit,dc=edu",
            filter => "entryUUID=".escape_filter_value($uuid),
            attrs => [ 'jpegPhoto' ],
            scope => 'one',
        )->shift_entry->get('jpegPhoto');
    } or warning $@;

    my $photo = $ret->[0];
    cache_set $uuid, $photo;

    return $photo;
}

get '/thumb/:dimensions/:UUID.jpg' => sub {
    my $uuid = param 'UUID';
    my $dim = param('dimensions');
    my $photo;

    return status 500 if $dim > 512 or $dim < 64;

    $photo = get_photo($uuid);
    return status 404 unless $photo;

    my $gd = GD::Thumbnail->new(square => 1);
    $photo = $gd->create($photo, $dim);

    send_file(\$photo, content_type => 'image/jpeg');
};

get '/:UUID.jpg' => sub {
    my $uuid = param 'UUID';
    my $photo;

    $photo = get_photo($uuid);
    return status 404 unless $photo;

    my $gd = GD::Thumbnail->new(square => 1);
    $photo = $gd->create($photo, 512);

    send_file(\$photo, content_type => 'image/jpeg');
};

get '/upload' => sub {
    my $user = request->header('X-WEBAUTH-USER');
    unless ($user) {
        return template 'error.tt', {
            message => 'Must log in with webauth',
        };
    }

    my $entryUUID;
    try {
        $entryUUID = ldap->search(
            base => "ou=Users,dc=csh,dc=rit,dc=edu",
            filter => "uid=".escape_filter_value($user),
            attrs => ['entryUUID'],
            scope => 'one',
        )->shift_entry->get('entryUUID')->[0];
    } or $entryUUID = 'error';

    return template 'upload.tt', {
        user => $user,
        entryUUID => $entryUUID
    };
};

post '/upload' => sub {
    my $ldap = ldap;
    my $user = request->header('X-WEBAUTH-USER') or
        return template 'error', {
            message => 'Must log in with webauth',
        };

    # Grab the photo from the request
    my $file = request->upload('photo') or
        return template 'error', {
            message => 'Must provide a photo'
        };

    # Check if the file is a jpeg
    if ($file->type ne "image/jpeg") {
        return template 'error', {
            message => 'Must provide a jpeg image'
        };
    }

    # Remove previous picture from cache
    my $uuid = ldap->search(
        base => "ou=Users,dc=csh,dc=rit,dc=edu",
        filter => "uid=$user",
        attrs => [ 'entryUUID' ],
        scope => 'one',
    )->shift_entry->get('entryUUID')->[0];
    cache_remove $uuid;

    my $gd = GD::Thumbnail->new(square => 1);
    my $photo = $gd->create($file->content, 512);

    # Make the change to LDAP
    my $ret = $ldap->modify("uid=$user,ou=Users,dc=csh,dc=rit,dc=edu",
        changes => [
            replace => [
                jpegPhoto => $photo,
            ],
        ],
    );
    cache_set $uuid, $photo;

    if ($ret->code) {
        warning $ret->error;
        return template 'error', {
            message => $ret->error,
        };
    }

    return template 'success', {
        message => 'Success!',
    };
};

true;
